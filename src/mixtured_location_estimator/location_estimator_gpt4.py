import openai
import time
import yaml

class Location_Estimator_GPT4:
    def __init__(self, api_key, prompt_path, robot_behavior_info, gpt_model="gpt-4"):
        """Main class for object-place estimator using GPT-4.

        Args:
            api_key (str): Path to the file that contains the api key
            gpt_model (str, optional): Pretrained GPT model. One of the following: 'gpt-4-1106-preview', 'gpt-4-vision-preview', 'gpt-4', 'gpt-4-32k', 'gpt-4-0613', 'gpt-4-32k-0613'.
        """

        # initialize
        self.load_api_key(api_key)
        self.model_name = gpt_model
        self.prompt_path = prompt_path
        self.robot_behavior_info = robot_behavior_info

    def estimate_location(self, target_obj, num_trial=10):
        """Estimate the location of the target object using GPT-4.
        
            """
        # log the start time
        start = time.time()
        # the number of succeed trials which means the total probability is over the threshold
        num_sampled = 0

        # the instruction prompt for GPT-4
        LOCATION_NAME_LIST = self.load_robot_behavior_info(self.robot_behavior_info)
        prompt = self.load_prompt(self.prompt_path)
        instruction = prompt.format(LOCATION_NAME_LIST=LOCATION_NAME_LIST)
        system = [{"role": "system", "content": instruction}]
        self.messages = system
        # the target prompt for GPT-4
        self.messages.append({"role": "user", "content": f"Object: {target_obj}"})
        self.messages.append({"role": "user", "content": f"Location: "})

        # the list to store the count of each place
        count_store = [0]*len(LOCATION_NAME_LIST)

        # loop over the number of trials
        for trial in range(num_trial):
            # temperature
            # The default is 1. The sampling temperature is specified between 0 and 2. 
            # Higher values, such as 0.8, make the output more random, while lower values, such as 0.2, make the output more concentrated and deterministic.
            response = openai.ChatCompletion.create(
                            model=self.model_name,
                            messages=self.messages,
                            temperature=1.0,
                            max_tokens=2048
                            )

            num_sampled += 1
            index = LOCATION_NAME_LIST.index(response['choices'][0]['message']['content'])
            count_store[index] += 1

        # Divide the number of times at each location by the number of sampled trials to obtain the probability.
        print(count_store)
        prob = [x / len(count_store) for x in count_store]

        return {'estimation': prob, 'locations': LOCATION_NAME_LIST, 'run_time': time.time()-start}


    def load_api_key(self, file_path):
        """
        Load the API key from the file.
        """

        with open(file_path, "r") as f:
            openai.api_key = f.read().strip()
        
    def load_prompt(self, file_path):
        """
        Load the prompt from the file.
        """
        with open(file_path, "r") as f:
            prompt = f.read()

        return prompt
    
    def load_robot_behavior_info(self, file_path):
        """
        Load the robot behavior information from the file.
        """
        with open(file_path, "r") as f:
            robot_behavior_info = yaml.load(f, Loader=yaml.SafeLoader)

        LOCATION_NAME_LIST = robot_behavior_info["LOCATION_NAME_LIST"]
        return LOCATION_NAME_LIST