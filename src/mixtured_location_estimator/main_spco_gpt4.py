#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import openai
import numpy as np
import csv
import os

result_path = "/root/HSR/catkin_ws/src/mixtured_location_estimator/src/result"

class MainSpCoGPT4():
        def __init__(self):
                pass

        def execute(self, target_name, result):
                object_name = target_name

                w_probs = [float(i) for i in result['estimation']]
                llm_w_list = [i for i in result['locations']]

                print("実際の予測結果:{}\n".format(result['estimation']))
                print("GPT-4側の単語辞書:{}\n".format(llm_w_list))
                print("GPT-4側の確率分布:{}\n".format(w_probs))

                # SpCoSLAM側の単語の辞書に合わせる処理
                with open(
                        '/root/HSR/catkin_ws/src/mixtured_location_estimator/src/spco/W_list.csv',
                        'r') as f:
                        reader = csv.reader(f)
                        for row in reader:
                                pass
                        place_names_spco = row
                place_names_spco.pop(-1)
                print("SpCo側の単語辞書:{}\n".format(place_names_spco))

                # SpCo側に含まれない単語の抽出
                diff = list(set(llm_w_list) - set(place_names_spco))

                # GPT-3側とSpCo側で統合した単語辞書
                integration_dict = place_names_spco + diff

                llm_probs_r = [0 for i in range(len(integration_dict))]  # 場所概念の単語数に合わせた表現
                for j in range(len(integration_dict)):
                        # GPT-4側にある単語のとき
                        if ((integration_dict[j] in llm_w_list) == True):
                                a = llm_w_list.index(integration_dict[j])
                                llm_probs_r[j] = w_probs[a]
                        # GPT-4側にないとき
                        else:
                                llm_probs_r[j] = 0

                print("統合した単語辞書:{}\n".format(integration_dict))
                print("統合した確率分布:{}\n".format(llm_probs_r))

                self.save_data(llm_probs_r, integration_dict, object_name)
                return


        def save_data(self, prob, w_list, object_name):
                # 推論結果をtxtでまとめて保存
                FilePath = result_path  + "/{}".format(object_name)
                # 推論結果をtxtでまとめて保存
                if not os.path.exists(FilePath):
                        os.makedirs(FilePath)

                # csvファイルで1行目に保存
                with open(FilePath + "/gpt4_inference_result.csv", 'w') as f:
                        writer = csv.writer(f, lineterminator='\n')
                        writer.writerow(prob)

                # 統合した単語辞書の保存
                with open(FilePath + "/integration_W_list.csv", 'w') as f:
                        writer = csv.writer(f, lineterminator='\n')
                        writer.writerow(w_list)

if __name__ == "__main__":
    srv = MainSpCoGPT4()
    srv.execute()

