#!/usr/bin/env python

import csv
import os
import numpy as np

data_path = '/root/HSR/catkin_ws/src/mixtured_location_estimator/src/spco'
result_path = "/root/HSR/catkin_ws/src/mixtured_location_estimator/src/result"

class CrossModalObject2PlaceForGPT4:

    def __init__(self):
        pass

    def main(self, target_name):
        pi, xi, theta_sw, object_name_list, place_name_list, integrated_place_names = self.read_data(target_name)
        place_name_list.pop(-1)

        """
        w_tのクロスモーダル推論
        P(w_t | o_t) = ∫ P(w_t | C_t) P(C_t | o_t) dC_t
        1. P(C_t | o_t) = P(C_t | π, o_t, ξ) = P(C_t | π) P(o_t | ξ, C_t)
        2. P(w_t | C_t, W)
        3. 周辺化した上で結果を出力させる
        命令された物体の名前と物体の辞書を対応させて、object_name_vectorを生成
        """

        target = object_name_list.index(target_name)
        object_name_vector = np.zeros(len(object_name_list))
        np.put(object_name_vector, [target], 1)

        prob_w_s_t = [0.0 for i in range(len(theta_sw[0]))]  # 場所の単語リスト作成
        for w in range(len(theta_sw[0])):
            for c in range(pi.size):
                prob = object_name_vector.dot(xi[c].T) * pi[c] * theta_sw[c][w]
                # P(o_t | xi_c^s) P(C^s | pi) P (w^s | theta^sw_c^s)
                prob_w_s_t[w] += prob

        prob_w_s_t_r = [float(j) / sum(prob_w_s_t) for j in prob_w_s_t]  # 正規化
        # print("Result of inference:")
        # print("{} = {}\n".format(place_name_list, prob_w_s_t_r))
        # print(prob_w_s_t_r)

        # GPT-4側の辞書と合わせる処理
        prob_w_integrated = [0 for i in range(len(integrated_place_names))] # 場所概念の単語数に合わせた表現
        for j in range(len(integrated_place_names)):
            # GPT-4側にある単語のとき
            if ((integrated_place_names[j] in place_name_list) == True):
                a = place_name_list.index(integrated_place_names[j])
                prob_w_integrated[j] = prob_w_s_t_r[a]
            # GPT-4側にないとき
            else:
                prob_w_integrated[j] = 0


        self.save_data(prob_w_integrated, target_name)
        inference_name = "crossmodal_inference_result"
        return

    def read_data(self, target_name):
        ## データの読み込み
        # π
        with open(data_path + '/pi.csv', 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
        pi_s_data = row
        del pi_s_data[-1]
        pi = np.array(pi_s_data, dtype=np.float64)
        # print("pi_s :{}\n".format(pi))

        # ξ
        xi = []
        with open(data_path + '/Xi.csv') as f:
            reader = csv.reader(f)
            for row in reader:
                del row[-1]
                xi.append(np.array(row, dtype=np.float64))
        xi = np.array(xi)
        # print("xi: {}\n".format(xi))

        # W
        theta_sw = []
        with open(data_path + '/W.csv') as f:
            reader = csv.reader(f)
            for row in reader:
                del row[-1]
                theta_sw.append(np.array(row, dtype=np.float64))
        # print("theta_sw: {}\n".format(theta_sw))

        # 場所の単語辞書
        with open(data_path + '/W_list.csv', 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
            place_name_list = row
            # del place_name_list[-1]
        # print(place_name_list)

        # 物体の単語辞書
        with open(data_path + '/Object_W_list.csv', 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
            object_name_list = row
            # del object_name_list[-1]
        # print(object_name_list)

        # 統合した単語辞書
        with open(result_path + '/{}/integration_W_list.csv'.format(target_name), 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
            integrated_place_name_list = row
            # del place_name_list[-1]
        # print(place_name_list)
        return pi, xi, theta_sw, object_name_list, place_name_list, integrated_place_name_list

    def save_data(self, prob, target_name):
        # 推論結果を保存
        path = result_path + '/{}'.format(target_name)
        if not os.path.exists(path):
            os.makedirs(path)

        # csvファイルで1行目に保存
        with open(path + "/crossmodal_inference_result.csv", 'w') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow(prob)
