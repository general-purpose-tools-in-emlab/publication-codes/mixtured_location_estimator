#!/usr/bin/env python

import csv
import numpy as np
import os

data_path = '/root/HSR/catkin_ws/src/mixtured_location_estimator/src/spco'
result_path = "/root/HSR/catkin_ws/src/mixtured_location_estimator/src/result"

class Object2GaussianIndexForGPT4:

    def __init__(self):
        pass

    def execute(self, inference_name, target_name):
        pi, theta_sw, phi, place_name_list, prob_w_o, integrated_place_name_list = self.read_data(inference_name, target_name)
        print(integrated_place_name_list)
        place_name_list.pop(-1)
        print(phi)
        # print(len(theta_sw[0]))
        # print(len(place_name_list))
        # print(type(prob_w_o[0][0]))
        """
        GPT4や重み平均の確率を考慮したP(i_t | O_t) <P(i_t | w_t) = P(i_t | w_t, O_t)を仮定>
        P(i_t | O_t) = ∑_{w} ( (∫ P(i_t | C_t) P(C_t | w_t) dC_t) * P(w_t | O_t) )
        1. prob = P(w_t | O_t)
        2. P(i_t | C_t) = P(i_t | Φ, C_t)
        3. P(C_t | w_t) = P(w_t | W, C_t) P(C_t | π)
        4. 周辺化させた結果を出力させる
        """

        prob_i_t_list = []

        ################################
        # ## ここでGPT-4で増えた要素分を0でパディングする
        # # 統合した辞書とSpCoの辞書での差分数を確認
        # diff = len(integrated_place_name_list) - len(place_name_list)
        # add_empty = np.zeros(diff)
        #
        # # パラメータWにadd_empty分を追加する
        # theta_sw_integrate = np.zeros((len(theta_sw), len(integrated_place_name_list)))
        # for l in range(len(theta_sw)):
        #     theta_sw_integrate[l] = np.insert(theta_sw[l], len(theta_sw[l]), add_empty)

        ################################

        ## P(i_t | w_t)の計算
        for i in range(len(theta_sw[0])):
            place_name_vector = np.zeros(len(theta_sw[0]))
            np.put(place_name_vector, [i], 1)

            prob_i_t = [0.0 for i in range(len(phi[0]))]  # 位置分布のindexリスト作成
            for j in range(len(phi[0])):
                for c in range(pi.size):
                    prob = place_name_vector.dot(theta_sw[c].T) * pi[c] * phi[c][j]
                    # P(w_t | W_c) P(C | pi) P (i_t | phi_c)
                    # print(prob)
                    prob_i_t[j] += prob

            # print(prob_i_t)
            prob_i_t_r = [float(k) / sum(prob_i_t) for k in prob_i_t]  # 正規化
            # print("Result of inference:")

            prob_i_t_list.append(prob_i_t_r)

        # print("P(i_t | w_t): {}".format(prob_i_t_list)) # 2-layer list

        ### uniform distrbでぱでぃんぐ###########
        diff = len(integrated_place_name_list) - len(place_name_list)
        add_empty = [1/pi.size] * pi.size
        for d in range(diff):
            prob_i_t_list.append(add_empty)
        ########################

        # print("P(i_t | w_t): {}".format(prob_i_t_list))  # 2-layer list

        ## ∑_{w} P(i_t | w_t) P(w_t | o_t)の計算
        # print(prob_w_o)
        # print(prob_i_t_list)
        print(len(phi[0]))
        print(phi[0])
        prob_i_o = [0.0 for i in range(len(phi[0]))]  # 位置分布のindexリスト作成
        print(prob_i_o)
        for i in range(len(phi[0])):
            for w in range(len(integrated_place_name_list)):
                prob = float(prob_w_o[0][w]) * prob_i_t_list[w][i]
                # print(prob)
                prob_i_o[i] += prob
            # print("OK")
        # print(prob_i_o)
        prob_i_o_r = [float(k) / sum(prob_i_o) for k in prob_i_o]  # 正規化
        print("P(i_t | O_t): {}".format(prob_i_o_r))
        mixture_ratio = prob_i_o_r
        self.save_data(prob_i_o_r)
        print(mixture_ratio)
        return mixture_ratio

    def read_data(self, inference_name, target_name):
        # π
        with open(data_path + '/pi.csv', 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
        pi_s_data = row
        del pi_s_data[-1]
        pi = np.array(pi_s_data, dtype=np.float64)
        # print("pi_s :{}\n".format(pi))

        # W
        theta_sw = []
        with open(data_path + '/W.csv') as f:
            reader = csv.reader(f)
            for row in reader:
                del row[-1]
                theta_sw.append(np.array(row, dtype=np.float64))
        # print("theta_sw: {}\n".format(theta_sw))

        # Φ
        phi = []
        with open(data_path + '/phi.csv') as f:
            reader = csv.reader(f)
            for row in reader:
                del row[-1]
                phi.append(np.array(row, dtype=np.float64))
        phi = np.array(phi)
        # print("phi: {}\n".format(phi))

        # 場所の単語辞書
        with open(data_path + '/W_list.csv', 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
            place_name_list = row
            # del place_name_list[-1]
        # print(place_name_list)

        with open(data_path + '/pi.csv', 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
        pi_s_data = row
        del pi_s_data[-1]
        pi = np.array(pi_s_data, dtype=np.float64)

        p_w_o = []
        # P (w_t | O_t)
        with open(result_path + '/{}.csv'.format(inference_name), 'r') as f:
            reader = csv.reader(f)
            p_w_o = [row for row in reader]
        # print(p_w_o)

        # 統合した単語辞書
        with open(result_path + '/{}/integration_W_list.csv'.format(target_name), 'r') as f:
            reader = csv.reader(f)
            for row in reader:
                pass
            integrated_place_name_list = row
            # del place_name_list[-1]


        return pi, theta_sw, phi, place_name_list, p_w_o, integrated_place_name_list

    def save_data(self, prob):
        # 推論結果をtxtでまとめて保存
        if not os.path.exists(result_path):
            os.makedirs(result_path)

        # csvファイルで1行目に保存
        with open(result_path + "/object_existence_prob.csv", 'w') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow(prob)
