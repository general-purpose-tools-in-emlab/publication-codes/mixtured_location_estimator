#!/usr/bin/env python

import csv
import os
import rospy
import numpy as np

eta = 0.50
result_path = "/root/HSR/catkin_ws/src/mixtured_location_estimator/src/result"


class WeightedAveragingForGPT4:

    def __init__(self):
        pass

    def execute(self, target_name):

        # 現場環境の学習結果
        data1 = np.loadtxt(result_path + "/{}/crossmodal_inference_result.csv".format(target_name), delimiter=",")
        rospy.loginfo("SpCoSLAM:{}".format(len(data1)))

        # GPT-4の結果
        data2 = np.loadtxt(result_path + "/{}/gpt4_inference_result.csv".format(target_name), delimiter=",")
        rospy.loginfo("GPT-4:{}".format(len(data2)))

        ### 重み平均プログラム
        weight_average_probs = (eta * np.asarray(data2)) + (
                (1 - eta) * np.asarray(data1))  # SpCo + GPT-4

        print("< Weight average processing Result >")
        print("****************************************************************\n")
        self.save_data(weight_average_probs)
        inference_name = "weighted_average_result"
        return

    def save_data(self, prob):
        # 推論結果を保存
        if not os.path.exists(result_path):
            os.makedirs(result_path)

        # csvファイルで1行目に保存
        with open(result_path + "/weighted_average_result.csv", 'w') as f:
            writer = csv.writer(f, lineterminator='\n')
            writer.writerow(prob)
