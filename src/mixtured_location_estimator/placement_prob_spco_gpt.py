#!/usr/bin/env python

import csv
import os
import numpy as np
from location_estimator_gpt4 import Location_Estimator_GPT4
from main_spco_gpt4 import MainSpCoGPT4
from crossmodal_object2place_for_gpt4 import CrossModalObject2PlaceForGPT4
from weighted_averaging_for_gpt4 import WeightedAveragingForGPT4
from object2gaussian_index_for_gpt4 import Object2GaussianIndexForGPT4

class OutputPlacementParobSpcoGpt(EventState):

    def __init__(self):
        pass

    # ==================================================================================================
    #
    #   Flexbe Methods
    #
    # ==================================================================================================
    def execute(self):
        self.target = "apple"
        self.inference_name = "weighted_average_result"

        # GPT-4の推論
        path_to_api_key = '/root/HSR/catkin_ws/src/mixtured_location_estimator/src/data/OPENAI_API_KEY.key'
        path_to_prompt = '/root/HSR/catkin_ws/src/mixtured_location_estimator/src/data/prompt.txt'
        path_to_robot_behavior_info = '/root/HSR/catkin_ws/src/mixtured_location_estimator/src/data/robot_behavior_info.yml'

        estimator = Location_Estimator_GPT4(path_to_api_key, path_to_prompt, path_to_robot_behavior_info)
        result = estimator.estimate_location(self.target)

        gpt4_inference = MainSpCoGPT4()
        gpt4_inference.execute(self.target, result)

        # 現場知識の推論
        spco_inference = CrossModalObject2PlaceForGPT4()
        spco_inference.main(self.target)

        # 重み付き平均の推論
        weighted_average = WeightedAveragingForGPT4()
        weighted_average.execute(self.target)

        # ガウス分布のindexの推論
        gaussian_inference = Object2GaussianIndexForGPT4()
        result = gaussian_inference.execute(self.inference_name, self.target)

        prob = result

        return